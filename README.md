# Bangla Fonts for your Linux Machine

Welcome to Bangla font installer for linux. This script is tested or maintained only for Debian based distributions like Ubuntu, Debian, Linux Mint, Deepin etc.

### Dependency

It depends on some tools which you have to allow it to install, it will ask.

- wget
- fontconfig

## Install:
```
wget --no-check-certificate https://raw.githubusercontent.com/fahadahammed/linux-bangla-fonts/master/font.sh -O font.sh;chmod +x font.sh;bash font.sh;rm font.sh
```

You can find an article in: https://fahadahammed.com/single-command-to-download-and-install-all-bangla-fonts-in-your-linux/
